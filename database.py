# -*- coding: utf-8 -*-
import sqlite3

class db_wrapper:

    def __init__(self, database):
        self.connection = sqlite3.connect(database)
        self.cursor = self.connection.cursor()

    def execute(self, code, params=None):
        with self.connection:
            try:
                return self.cursor.execute(code, params).fetchall()
            except sqlite3.Error as er:
                return False


    def select_all(self):
        with self.connection:
            try:
                return self.cursor.execute('SELECT * FROM users')
            except sqlite3.Error as er:
                return False


    def add_user(self, chat_id):
        return self.execute("INSERT INTO users (chat_id) values(?)", str(chat_id))

    def get_user(self, chat_id):
        return self.execute('SELECT * FROM users WHERE chat_id = ?', str(chat_id))


    def set_user_level(self, chat_id, level):
        return self.execute("UPDATE users SET level = ? WHERE chat_id = ?", (str(level),str(chat_id)))

    def set_word_count(self, chat_id, word_count):
        return self.execute("UPDATE users SET word_count = ? WHERE chat_id = ?", (str(word_count),str(chat_id)))


    def close(self):
        """ Закрываем текущее соединение с БД """
        self.connection.close()