# -*- coding: utf-8 -*-
import config
import telebot
from telebot import types
import time
import database
import dictionary
import datetime
import random
#static
bot = telebot.TeleBot(config.token)

menu_k = types.InlineKeyboardMarkup()
menu_k.add(types.InlineKeyboardButton(text="Настройки", callback_data="settings"))
menu_k.add(types.InlineKeyboardButton(text="✖", callback_data="close"))

settings_k = types.InlineKeyboardMarkup()
settings_k.add(
    types.InlineKeyboardButton(text="Устоновить количество слов в сутки", callback_data="set_day_word_count"))
settings_k.add(types.InlineKeyboardButton(text="Устоновить уровень владения языком", callback_data="set_lang_level"))
settings_k.add(types.InlineKeyboardButton(text="← Назад", callback_data="menu"))





def clear_next_step_handlers(chat_id):
    if chat_id in bot.pre_message_subscribers_next_step:
        bot.pre_message_subscribers_next_step[chat_id].clear()


#Начало
@bot.message_handler(commands=["start"])
def start_command(message):
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(text="Открыть меню", callback_data="menu"))
    db = database.db_wrapper(config.database_name)

    if db.add_user(message.chat.id):
        bot.send_message(message.chat.id, "Добро пожаловать!", reply_markup=keyboard)
    else:
        bot.send_message(message.chat.id, "Я уже начинал с вами общение 😊", reply_markup=keyboard)
    db.close()

#Помощь
@bot.message_handler(commands=["help", "menu"])
def help_command(message):
    bot.send_message(message.chat.id, "Моё меню:", reply_markup=menu_k)


#Наебнуть бота #TODO не работает, разобраться!
'''@bot.message_handler(commands=["exit"])
def help_command(message):
    bot.send_message(message.chat.id, "Ок!")
    k = 1/0'''



def send_new_word(chat_id, level = 1):

    out = dictionary.Create_Article(level)
    # TODO кнопка расширение статьи
    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(text="Расширенная статья", callback_data="getsample"))
    bot.send_message(chat_id, text=out, parse_mode="HTML")

#Вернуть статью о слове
@bot.message_handler(commands=["word"])
def help_command(message):
    def isint(s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    mes = message.text.split(' ')
    level = 1
    if len(mes) > 1 and isint(mes[1]):
        level = int(mes[1])
    if level > 6:
        level = 6
    if level < 1:
        level = 1
    send_new_word(message.chat.id, level)

#Калбак для устоновки количества слов в сутки
def set_word_count(message):
    def isint(s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    keyboard = types.InlineKeyboardMarkup()
    keyboard.add(types.InlineKeyboardButton(text="← Назад", callback_data="settings"))

    if (isint(message.text) and 1 <= int(message.text) and int(message.text) <= 50):
        db = database.db_wrapper(config.database_name)
        k = int(message.text)
        db.set_word_count(message.chat.id, k*1000)
        db.close()

        bot.send_message(message.chat.id, "Сохранено 👍", reply_markup=keyboard)
    else:
        bot.send_message(message.chat.id, "*Неверный формат! (число от 1 до 50)*", parse_mode="Markdown", reply_markup=keyboard)



#Оброботчик кнопочек
@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    # Если сообщение из чата с ботом
    if call.message:
        if call.data == "menu":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Моё меню:", reply_markup=menu_k)
        elif call.data == "close":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="...")

        elif call.data == "settings":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Настройки", reply_markup=settings_k)

            clear_next_step_handlers(call.message.chat.id)


        elif call.data == "set_day_word_count":
            keyboard = types.InlineKeyboardMarkup()
            keyboard.add(types.InlineKeyboardButton(text="← Назад", callback_data="settings"))
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="*Введите количество присылаемых слов в сутки (от 1 до 50):* ",
                                  parse_mode="Markdown",
                                  reply_markup=keyboard)
            bot.register_next_step_handler(call.message, set_word_count, )




        elif call.data == "set_lang_level":
            keyboard = types.InlineKeyboardMarkup()
            keyboard.add(types.InlineKeyboardButton(text="Beginner, Elementary (Начальный)", callback_data="level_1")
                         ,types.InlineKeyboardButton(text="Pre-Intermediate (Ниже среднего)", callback_data="level_2"))
            keyboard.add(types.InlineKeyboardButton(text="Intermediate (Средний)", callback_data="level_3")
                         ,types.InlineKeyboardButton(text="Upper-Intermediate (Выше среднего)", callback_data="level_4"))
            keyboard.add(types.InlineKeyboardButton(text="Advanced (Продвинутый)", callback_data="level_5")
                         ,types.InlineKeyboardButton(text="Proficiency (Свободное владение)", callback_data="level_6"))
            keyboard.add(types.InlineKeyboardButton(text="← Назад", callback_data="settings"))
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Выберете ваш уровень владения, чтобы я мог прислать вам новые слова:",
                                  reply_markup=keyboard)

        elif call.data[0:5] == "level":
            keyboard = types.InlineKeyboardMarkup()
            keyboard.add(types.InlineKeyboardButton(text="← Назад", callback_data="settings"))

            level = int(call.data[6])
            db = database.db_wrapper(config.database_name)
            db.set_user_level(call.message.chat.id, level)
            db.close()
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Готово 👍",
                                  reply_markup=keyboard)

def LaunchBot():
    bot.polling(none_stop=True)



def GetSecondsToEndDay():
    now = datetime.datetime.now()
    #print(str(now.hour) + " " + str(now.minute) + " " + str(now.second))
    return (23 - now.hour)*3600 + (60 - now.minute)*60 + (60 - now.second)

def UpdateBot():
    db = database.db_wrapper(config.database_name)
    while(True):
        #ToEndDay = GetSecondsToEndDay()/61
        probability = random.random()
        for user in db.select_all():
            k = user[3]/1000
            #r = k - (user[3] - k*1000)
            p = k/1416.0
            if probability < p:
                db.set_word_count(user[1], user[3]+1)
                send_new_word(user[1], user[2])


        time.sleep(61)
    db.close()