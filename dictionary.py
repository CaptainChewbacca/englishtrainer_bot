import config
import database
import random
import http.client
import json
import urllib.parse



PartsOfSpeech = {
	"noun" : "сущ.",
	"pronoun" : "мест.",
	"verb" : "глагол",
	"adjective" : "прил.",
	"adverb" : "наречие",
	"preposition" : "предлог",
	"interjection" : "междометие",
	"participle" : "причастие"
}


def get_random_word(level):

	k = level*level * 1000
	i = k + round(random.betavariate(1.21, 1.21)*k) - k/2
	db_reader = database.db_wrapper(config.database_name)
	out = db_reader.execute('SELECT * FROM words_1 WHERE id = ?', (i + level * 500,))
	if out:
		return out[0][1]
	else:
		return False


def Request(word):
	headers = {'Content-Type': 'application/x-www-form-urlencoded'}
	requestString = 'key=' + config.yandex_dictionary + '&lang=en-ru&text=' + word
	ApiConn = http.client.HTTPSConnection('dictionary.yandex.net', 443)
	ApiConn.request('POST', '/api/v1/dicservice.json/lookup', requestString, headers)
	r = ApiConn.getresponse().read().decode()
	ApiConn.close()
	return json.loads(r)

def Create_Article(level):
	out = Load_Random_Word(level)
	res = "<b>" + out[0]["text"] + "</b> ["+out[0]["ts"] + "] "
	res += "\n"
	for i in range(len(out)):
		res += "<code>" + str(i + 1) + ")</code>"

		lastparts = ""
		for j in range(len(out[i]["tr"])):
			if j > 0:
				res += "     "
			res += " " + out[i]["tr"][j]["text"]


			if "syn" in out[i]["tr"][j]:
				res += " ; "
				for k in range(len(out[i]["tr"][j]["syn"])):
					res += "<i>"+out[i]["tr"][j]["syn"][k]["text"]+" ; </i>"


			newparts = PartsOfSpeech.get(out[i]["tr"][j]["pos"], "")
			if lastparts != newparts:
				res += " <code>" + newparts + "</code>" + '\n'
				lastparts = newparts
			else:
				res += '\n'

			if "ex" in out[i]["tr"][j]:
				res += "<code>"
				for k in range(len(out[i]["tr"][j]["ex"])):
					res += "      " + out[i]["tr"][j]["ex"][k]["text"] + " - \n"
					res += "         " + out[i]["tr"][j]["ex"][k]["tr"][0]["text"] + " \n"
				res += "</code>"

	#print(out[0]["text"])
	return res


def Load_Random_Word(level):
	repeat = True
	while(repeat):
		word = get_random_word(level)
		if word != False:
			out = Request(word)['def']
			if len(out) > 0:
				return out
			else:
				repeat = True
		else:
			repeat = True

	return False
